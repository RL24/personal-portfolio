import { TestBed, async, ComponentFixture } from '@angular/core/testing';

import { AppComponent } from './app.component';
import { SkillComponent } from './skill/skill.component';
import { ProjectIconComponent } from './home/project-icon/project-icon.component';
import { RouterModule } from '@angular/router';
import { ProjectComponent } from './projects/project/project.component';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';

describe('AppComponent', () => {
  let component: AppComponent;
  let fixture: ComponentFixture<AppComponent>;
  let compiled: HTMLElement;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AppComponent, SkillComponent, ProjectIconComponent, ProjectComponent ],
      imports: [
        RouterModule.forRoot([]),
        FontAwesomeModule
      ]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AppComponent);
    
    component = fixture.componentInstance;

    fixture.detectChanges();
    compiled = fixture.nativeElement;
  });

  it('should create the app', () => {
    expect(component).toBeTruthy();
  });

  it('should have a profile', () => {
    expect(component.profile).toBeTruthy();
  });

  it('should have a profile name', () => {
    expect(component.profile.name).toEqual('Richard Lapwood');
  });

  it('should have a profile role', () => {
    expect(component.profile.role).toEqual('Software Developer');
  });

  it('should have profile skills', () => {
    expect(component.profile.skills).toBeTruthy();
    expect(component.profile.skills.length).toBeGreaterThan(0);
  });

  it('should have profile social links', () => {
    expect(component.profile.social).toBeTruthy();
    expect(component.profile.social.length).toBeGreaterThan(0);
  });

  it('should render profile picture', () => {
    const profilePicture: Element = compiled.querySelector('#profile-picture');
    expect(profilePicture).toBeTruthy();
    expect(profilePicture.getAttribute('src')).toEqual('/assets/images/profile.webp');
    expect(profilePicture.getAttribute('alt')).toEqual(component.profile.name);
  });

  it('should render certification ac-jpa', () => {
    const certification: Element = compiled.querySelector('#certification-ac-jpa');
    expect(certification).toBeTruthy();
    expect(certification.getAttribute('src')).toEqual('/assets/images/certifications/ac-jpa.webp');
    expect(certification.getAttribute('alt')).toEqual('ACP-600 Project Administration in Jira Server');
  });

  it('should render certification acp-jsw', () => {
    const certification: Element = compiled.querySelector('#certification-acp-jsw');
    expect(certification).toBeTruthy();
    expect(certification.getAttribute('src')).toEqual('/assets/images/certifications/acp-jsw.webp');
    expect(certification.getAttribute('alt')).toEqual('ACP-300 Agile Development with Jira Software');
  });

  it('should render name', () => {
    const name: Element = compiled.querySelector('.name');
    expect(name).toBeTruthy();
    expect(name.textContent).toEqual(component.profile.name);
  });

  it('should render role', () => {
    const role: Element = compiled.querySelector('.role');
    expect(role).toBeTruthy();
    expect(role.textContent).toEqual(component.profile.role);
  });

  it('should render social links', () => {
    const socialLinks: NodeListOf<Element> = compiled.querySelectorAll('.social-container a');
    expect(socialLinks.length).toEqual(component.profile.social.length);

    for (let i = 0; i < socialLinks.length; i++) {
      expect(socialLinks[i].getAttribute('id')).toContain(component.profile.social[i].id);
      expect(socialLinks[i].getAttribute('href')).toEqual(component.profile.social[i].url);
      expect(socialLinks[i].getAttribute('aria-label')).toEqual(component.profile.social[i].title);

      const icon = socialLinks[i].querySelector('fa-icon');
      expect(icon).toBeTruthy();
    }
  });

  it('should render skills', () => {
    const skills: NodeListOf<Element> = compiled.querySelectorAll('app-skill');
    expect(skills.length).toEqual(component.profile.skills.length);
  });
});
