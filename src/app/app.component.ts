import { Component, ViewEncapsulation } from '@angular/core';
import { Profile } from './models/profile.model';
import { Skill } from './models/skill.model';
import { Color } from './models/color.enum';
import { SocialMedia } from './models/social.model';

import { faLinkedin, faGithubSquare } from '@fortawesome/free-brands-svg-icons';
import { faEnvelopeSquare, faPhoneSquare } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class AppComponent {

  profile: Profile = {
    name: 'Richard Lapwood',
    role: 'Software Developer',
    skills: [
      new Skill('java', 'Java', Color.RED, 90),
      new Skill('html', 'HTML', Color.RED, 70),
      new Skill('cs', 'C#', Color.PURPLE, 65),
      new Skill('ts', 'TypeScript', Color.BLUE, 65),
      new Skill('js', 'JavaScript', Color.YELLOW, 60),
      new Skill('css', 'CSS', Color.BLUE, 55),
      new Skill('angular', 'Angular', Color.RED, 55),
      new Skill('nodejs', 'NodeJS', Color.GREEN, 50),
      new Skill('postgresql', 'PostgreSQL', Color.BLUE, 45),
      new Skill('mysql', 'MySQL', Color.BLUE, 45),
      new Skill('atlassdk', 'AtlasSDK', Color.BLUE, 45),
      new Skill('sass', 'SASS', Color.PINK, 40),
      new Skill('pug', 'PugJS', Color.YELLOW, 40),
      new Skill('react', 'React', Color.PURPLE, 35),
      new Skill('cpp', 'C++', Color.BLUE, 30),
      new Skill('gradle', 'Gradle', Color.GREEN, 25),
      new Skill('graphql', 'GraphQL', Color.PINK, 25),
      new Skill('php', 'PHP', Color.PURPLE, 25),
      new Skill('webpack', 'Webpack', Color.BLUE, 20),
      new Skill('apollo', 'Apollo', Color.BLUE, 20),
      new Skill('selenium', 'Selenium', Color.GREEN, 20),
      new Skill('firebase', 'FireBase', Color.YELLOW, 15),
      new Skill('cmake', 'CMake', Color.GREEN, 15),
      new Skill('docker', 'Docker', Color.BLUE, 10),
      new Skill('aws', 'AWS', Color.YELLOW, 10)
    ],
    social: [
      new SocialMedia('linkedin', 'LinkedIn', 'https://linkedin.com/in/richardlapwood/', faLinkedin),
      new SocialMedia('github', 'GitHub', 'https://github.com/RL24', faGithubSquare),
      new SocialMedia('email', 'Email', 'mailto:richardlapwood24@gmail.com', faEnvelopeSquare),
      new SocialMedia('phone', 'Phone', 'tel:+64220369958', faPhoneSquare)
    ]
  };
}
