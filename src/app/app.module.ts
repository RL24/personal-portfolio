import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { ProjectsComponent } from './projects/projects.component';
import { ProjectIconComponent } from './home/project-icon/project-icon.component';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { ProjectsModule } from './projects/projects.module';

const routes: Routes = [{
  path: '',
  component: HomeComponent
}, {
  path: 'project',
  loadChildren: () => import('./projects/projects.module').then((m) => m.ProjectsModule)
}, {
  path: '**', redirectTo: ''
}]

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    ProjectsComponent,
    ProjectIconComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    RouterModule.forRoot(routes),
    FontAwesomeModule,
    ProjectsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
