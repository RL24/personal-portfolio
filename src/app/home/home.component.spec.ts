import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HomeComponent } from './home.component';
import { ProjectIconComponent } from './project-icon/project-icon.component';
import { ProjectComponent } from '../projects/project/project.component';
import { RouterModule } from '@angular/router';

describe('HomeComponent', () => {
  let component: HomeComponent;
  let fixture: ComponentFixture<HomeComponent>;
  let compiled: HTMLElement;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HomeComponent, ProjectIconComponent ],
      imports: [
        RouterModule.forRoot([])
      ]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HomeComponent);
    
    component = fixture.componentInstance;

    fixture.detectChanges();
    compiled = fixture.nativeElement;
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should have projects', () => {
    expect(component.projects).toBeTruthy();
  });

  it('should have more than 0 projects', () => {
    expect(component.projects.length).toBeGreaterThan(0);
  })

  it('should render projects', () => {
    const projects: NodeListOf<Element> = compiled.querySelectorAll('app-project-icon');
    expect(projects.length).toEqual(component.projects.length);
  });

});
