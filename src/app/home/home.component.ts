import { Component, ViewEncapsulation } from '@angular/core';
import { Project } from '../models/project.model';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class HomeComponent {
  public projects: Project[] = [
    new Project('portfolio', 'Portfolio', 'My Portfolio Website'),
    new Project('mymahi', 'MyMahi', 'Commercial Web Service'),
    new Project('minecraft', 'Minecraft', 'Minecraft Related Projects'),
    new Project('tukbox', 'TukBox', 'Basic Android App Demo'),
    new Project('expressjs', 'ExpressJS NewsFeed', 'Basic ExpressJS Demo'),
    new Project('sudokusolver', 'Sudoku Solver', 'C++ Sudoku Solver'),
    new Project('thegraphicshop', 'The Graphic Shop', 'Business Landing Page'),
    new Project('harleycartel', 'Harley Cartel', 'Under Construction')
  ];

}
