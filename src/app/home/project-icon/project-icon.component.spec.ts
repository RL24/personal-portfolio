import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProjectIconComponent } from './project-icon.component';
import { Project } from 'src/app/models/project.model';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterModule } from '@angular/router';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { ProjectsModule } from 'src/app/projects/projects.module';

describe('ProjectIconComponent', () => {
  let component: ProjectIconComponent;
  let fixture: ComponentFixture<ProjectIconComponent>;
  let compiled: HTMLElement;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProjectIconComponent ],
      imports: [
        RouterModule.forRoot([])
      ]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProjectIconComponent);
    
    component = fixture.componentInstance;
    component.project = new Project('testId', 'Test Title', 'Test project description', 'Test project modal');

    fixture.detectChanges();
    compiled = fixture.nativeElement;
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should have a project', () => {
    expect(component.project).toBeTruthy();
  });

  it('should render project', () => {
    const project: Element = compiled.querySelector('a');
    expect(project).toBeTruthy();
  });

  it('should render project with id', () => {
    expect(compiled.querySelector('a').getAttribute('id')).toEqual(`project-${component.project.id}`);
  });

  it('should render project with href', () => {
    expect(compiled.querySelector('a').getAttribute('href')).toEqual(`/project/${component.project.id}`);
  });

  it('should render background', () => {
    const projectDiv: HTMLElement = compiled.querySelector('.project');
    expect(projectDiv).toBeTruthy();
    expect(projectDiv.style.backgroundImage).toEqual(`url("${component.project.image}")`);
  });

  it('should render title', () => {
    expect(compiled.querySelector('.title').textContent).toEqual(component.project.title);
  });

  it('should render description', () => {
    expect(compiled.querySelector('.subtitle').textContent).toEqual(component.project.description);
  });
  
});
