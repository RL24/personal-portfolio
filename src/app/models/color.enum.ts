export enum Color {
  RED = '226, 124, 113',
  YELLOW = '233, 225, 136',
  GREEN = '118, 235, 114',
  BLUE = '105, 171, 221',
  PURPLE = '139, 120, 224',
  PINK = '233, 127, 213'
}