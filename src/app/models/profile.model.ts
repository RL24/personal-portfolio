import { Skill } from './skill.model';
import { SocialMedia } from './social.model';

export interface Profile {
  name: string;
  role: string;
  skills: Skill[];
  social: SocialMedia[];
}