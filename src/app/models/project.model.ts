import { getProject } from '../util/io.helper';

export class Project {
    id: string;
    title: string;
    image: string;
    url: string;
    description: string;
    modalMessage?: string;

    constructor(id: string, title: string, description: string, modalMessage?: string) {
        this.id = id;
        this.title = title;
        this.image = getProject(id);
        this.url = `/${id}`;
        this.description = description;
        this.modalMessage = modalMessage;
    }
}