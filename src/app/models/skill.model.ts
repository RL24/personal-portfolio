import { Color } from './color.enum';
import { getTool } from '../util/io.helper';

export class Skill {
  id: string;
  title: string;
  color: Color;
  image: string;
  level?: number;

  constructor(id: string, title: string, color: Color, level?: number) {
    this.id = id;
    this.title = title;
    this.color = color;
    this.image = getTool(id);
    this.level = level;
  }
}