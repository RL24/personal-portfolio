import { IconDefinition } from '@fortawesome/fontawesome-svg-core';

export class SocialMedia {
  id: string;
  title: string;
  url: string;
  icon: IconDefinition;

  constructor(id: string, title: string, url: string, icon: IconDefinition) {
    this.id = id;
    this.title = title;
    this.url = url;
    this.icon = icon;
  }
}