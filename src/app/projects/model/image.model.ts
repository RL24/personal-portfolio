import { getGalleryImage } from '../../util/io.helper';

export class Image {
    id: string;
    title: string;
    thumbUrl: string;
    url: string;

    constructor(id: string, folder: string, title: string) {
        this.id = id;
        this.title = title;
        this.thumbUrl = getGalleryImage(folder, true, id);
        this.url = getGalleryImage(folder, false, id);
    }
}