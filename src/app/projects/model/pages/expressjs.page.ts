import { Skill } from '../../../models/skill.model';
import { Color } from '../../../models/color.enum';

import { ProjectPage } from '../project.model';
import { Technology } from '../technology.model';
import { Image } from '../image.model';

export class ExpressJSPage implements ProjectPage {
    id: string = 'expressjs';
    title: string = 'ExpressJS Newsfeed';
    url: string = '//github.com/RL24/I311';
    description: string[] = [
        'During my time at UCOL, we were tasked to take a current web technology and ' +
        'provide a presentation and demo of the technology in use. For my assignment, I chose ' +
        'NodeJS and ExpressJS to create a demo "newsfeed" type web application.',

        'Inspiration was taken from Facebook\'s profile newsfeed, except the process of my ' +
        'version was to exclude the requirement of adding people as friends.'
    ];
    technologies: Technology[] = [
        new Technology(new Skill('css', 'CSS', Color.BLUE), '//en.wikipedia.org/wiki/Cascading_Style_Sheets'),
        new Technology(new Skill('js', 'JavaScript', Color.YELLOW), '//javascript.com'),
        new Technology(new Skill('mysql', 'MySQL', Color.BLUE), '//mysql.com'),
        new Technology(new Skill('nodejs', 'NodeJS', Color.GREEN), '//nodejs.org'),
        new Technology(new Skill('pug', 'PugJS', Color.YELLOW), '//pugjs.org'),
    ];
    images: Image[] = [];
}