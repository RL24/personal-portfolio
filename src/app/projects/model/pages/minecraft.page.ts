import { Skill } from '../../../models/skill.model';
import { Color } from '../../../models/color.enum';

import { ProjectPage } from '../project.model';
import { Technology } from '../technology.model';
import { Image } from '../image.model';

export class MinecraftPage implements ProjectPage {
    id: string = 'minecraft';
    title: string = 'Minecraft Projects';
    description: string[] = [
        'A vast majority of my spare time from 2012 to 2018 was spent across working on various ' +
        'Minecraft projects, from server plugins, through ingame build projects, to client modifications.' +
        'This time spent on Minecraft projects was a large part of where my passion for programming ' +
        'originated. I spent nearly every waking hour outside of highschool and UCOL working on ' +
        'anything Minecraft related.',

        'These projects ranged across languages, from Java being the base language the game was also ' +
        'written in, making modding easier as Java decompile / bytecode manipulation is far easier than ' +
        'other compiled languages, to C# and C++ injection making use of JNI and JVMTI. Several projects ' +
        'also made use of Java agents via the Java Attach API.',

        'For personal safety reasons I will not be disclosing links to any repositories regarding these projects, ' +
        'unless explicitly requested.'
    ];
    technologies: Technology[] = [
        new Technology(new Skill('cs', 'C#', Color.PURPLE), '//en.wikipedia.org/wiki/C_Sharp_(programming_language)'),
        new Technology(new Skill('cpp', 'C++', Color.BLUE), '//en.wikipedia.org/wiki/C%2B%2B'),
        new Technology(new Skill('forge', 'Forge', Color.PURPLE), '//minecraftforge.net'),
        new Technology(new Skill('gradle', 'Gradle', Color.GREEN), '//gradle.org'),
        new Technology(new Skill('java', 'Java', Color.RED), '//java.com'),
        new Technology(new Skill('minecraft', 'Minecraft', Color.GREEN), '//minecraft.net'),
    ];
    images: Image[] = [
    ];
}