import { Skill } from '../../../models/skill.model';
import { Color } from '../../../models/color.enum';

import { ProjectPage } from '../project.model';
import { Technology } from '../technology.model';
import { Image } from '../image.model';

export class MyMahiPage implements ProjectPage {
    id: string = 'mymahi';
    title: string = 'MyMahi';
    url: string = '//mymahi.co.nz';
    description: string[] = [
        'MyMahi started as a concept idea under the name MyBlueprint, and throughout the final year ' +
        'of my ICT degree, it became my final paper, being an \'Industry Project\', and was eventually ' +
        'a working functional web service. I was primarily tasked with implementing the front-end ' +
        'solution, but occasionally given access to working on the back-end stack.',

        'MyMahi is now a nationally used web service to help students plan their future career pathway plan. ' +
        'The front-end implementation has since been improved with new features, but still resembles my ' +
        'initial implementation during my final year at UCOL.',

        'The screenshots below are from a very early stage of development, as the UI design has since changed.'
    ];
    technologies: Technology[] = [
        new Technology(new Skill('angular', 'Angular', Color.RED), '//angular.io'),
        new Technology(new Skill('apollo', 'Apollo', Color.BLUE), '//apollographql.com'),
        new Technology(new Skill('aws', 'AWS', Color.YELLOW), '//aws.amazon.com'),
        new Technology(new Skill('github', 'GitHub', Color.PURPLE), '//github.com'),
        new Technology(new Skill('graphql', 'GraphQL', Color.PINK), '//graphql.org'),
        new Technology(new Skill('html', 'HTML', Color.RED), '//en.wikipedia.org/wiki/HTML5'),
        new Technology(new Skill('postgresql', 'PostgreSQL', Color.BLUE), '//postgresql.org'),
        new Technology(new Skill('sass', 'SASS', Color.PINK), '//sass-lang.com'),
        new Technology(new Skill('ts', 'TypeScript', Color.BLUE), '//typescriptlang.org')
    ];
    images: Image[] = [
        new Image('student_newsfeed', this.id, 'Student Newsfeed'),
        new Image('student_goals', this.id, 'Student Goals'),
        new Image('student_profile', this.id, 'Student Profile'),
        new Image('teacher_groups', this.id, 'Teacher Groups'),
        new Image('teacher_student', this.id, 'Teacher Student'),
        new Image('teacher_student_goals', this.id, 'Teacher Student Goals'),
        new Image('workready_life_skills', this.id, 'WorkReady Life Skills'),
        new Image('workready_licences_held', this.id, 'WorkReady Licences Held'),
        new Image('workready_review', this.id, 'WorkReady Review')
    ];
}