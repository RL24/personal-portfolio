import { Skill } from '../../../models/skill.model';
import { Color } from '../../../models/color.enum';

import { ProjectPage } from '../project.model';
import { Technology } from '../technology.model';
import { Image } from '../image.model';

export class PortfolioPage implements ProjectPage {
    id: string = 'myportfolio';
    title: string = 'My Portfolio';
    url: string = 'https://gitlab.com/RL24/personal-portfolio';
    description: string[] = [
        'This website was designed to be a central location for people to view my current skillset, ' +
        'and any projects I have either completed, or are currently in the process of developing. ' +
        'I will never claim myself as a designer, but I like to think I can distinguish between messy, and clean.',

        'The biggest challenge when making this portfolio, was coming up with a design that I liked, ' +
        'but also had a somewhat minimal feel to it. Adding the slight animations, e.g. when hovering ' +
        'the skills, projects, or social links, gave it so much more personality in my opinion.'
    ];
    technologies: Technology[] = [
        new Technology(new Skill('angular', 'Angular', Color.RED), '//angular.io'),
        new Technology(new Skill('gitlab', 'GitLab', Color.RED), '//gitlab.com'),
        new Technology(new Skill('html', 'HTML', Color.RED), '//en.wikipedia.org/wiki/HTML5'),
        new Technology(new Skill('sass', 'SASS', Color.PINK), '//sass-lang.com'),
        new Technology(new Skill('selenium', 'Selenium', Color.GREEN), '//selenium.dev'),
        new Technology(new Skill('ts', 'TypeScript', Color.BLUE), '//typescriptlang.org'),
        new Technology(new Skill('webpack', 'Webpack', Color.BLUE), '//webpack.js.org'),
        new Technology(new Skill('webp', 'WebP', Color.GREEN), '//developers.google.com/speed/webp')
    ];
    images: Image[] = [
        new Image('home', 'portfolio', 'Home'),
        new Image('project', 'portfolio', 'Project'),
        new Image('tablet', 'portfolio', 'Tablet'),
        new Image('mobile', 'portfolio', 'Mobile')
    ];
}