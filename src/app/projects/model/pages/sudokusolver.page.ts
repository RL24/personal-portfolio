import { Skill } from '../../../models/skill.model';
import { Color } from '../../../models/color.enum';

import { ProjectPage } from '../project.model';
import { Technology } from '../technology.model';
import { Image } from '../image.model';

export class SudokuSolverPage implements ProjectPage {
    id: string = 'sudokusolver';
    title: string = 'C++ Sudoku Solver';
    url: string = '//github.com/RL24/I309';
    description: string[] = [
        'During my time at UCOL, I was given the option to choose something meaningful ' +
        'to learn, that was also approved by my supervisor, so I chose to learn C++ ' +
        'as it has always been a language I wanted to learn more in depth rather than ' +
        'the generic programming practices.',

        'To pass this selective assignment, I decided the best way for me to learn ' +
        'C++ was to develop a sudoku solver, something that can be made in virtually ' +
        'any language, but is still complex enough to use most of the selected language\'s ' +
        'features.'
    ];
    technologies: Technology[] = [
        new Technology(new Skill('cmake', 'CMake', Color.GREEN), '//cmake.org'),
        new Technology(new Skill('cpp', 'C++', Color.BLUE), '//en.wikipedia.org/wiki/C%2B%2B')
    ];
    images: Image[] = [];
}