import { Skill } from '../../../models/skill.model';
import { Color } from '../../../models/color.enum';

import { ProjectPage } from '../project.model';
import { Technology } from '../technology.model';
import { Image } from '../image.model';

export class TheGraphicShopPage implements ProjectPage {
    id: string = 'thegraphicshop';
    title: string = 'The Graphic Shop';
    url: string = '//thegraphicshop.co.nz';
    description: string[] = [
        'The Graphic Shop is a signwriting company that had an outdated website from 2014, ' +
        'so I took it into my own hands to update to something a little more modern. The idea ' +
        'of this landing page was to have important information quickly accessible by potential ' +
        'and current customers.'
    ];
    technologies: Technology[] = [
        new Technology(new Skill('css', 'CSS', Color.BLUE), '//en.wikipedia.org/wiki/Cascading_Style_Sheets'),
        new Technology(new Skill('html', 'HTML', Color.RED), '//en.wikipedia.org/wiki/HTML5'),
        new Technology(new Skill('js', 'JavaScript', Color.RED), '//javascript.com'),
        new Technology(new Skill('php', 'PHP', Color.BLUE), '//php.net'),
    ];
    images: Image[] = [
        new Image('home', this.id, 'Home'),
        new Image('services', this.id, 'Services'),
    ];
}