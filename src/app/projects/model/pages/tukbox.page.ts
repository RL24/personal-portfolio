import { Skill } from '../../../models/skill.model';
import { Color } from '../../../models/color.enum';

import { ProjectPage } from '../project.model';
import { Technology } from '../technology.model';
import { Image } from '../image.model';

export class TukBoxPage implements ProjectPage {
    id: string = 'tukbox';
    title: string = 'TukBox';
    url: string = '//github.com/RL24/D303';
    description: string[] = [
        'During my time at UCOL, we were tasked to create an Android application ' +
        'developed in Android Studio, and made use of Firebase as database ' +
        'storage / processing. This was by far my favorite assignment as at the time, ' +
        'Java was my prominent language, so I was submerged in my comfort zone.',

        'This mobile application was my first interaction with SAML Identity Providers, ' +
        'where we had to configure Google authentication via Firebase. We were also ' +
        'challenged to create Firebase push notifications, which at first seemed to be ' + 
        'more complicated, but after a short while figured out the configuration and had ' +
        'push notifications working in no time.'
    ];
    technologies: Technology[] = [
        new Technology(new Skill('java', 'Java', Color.RED), '//java.com'),
        new Technology(new Skill('firebase', 'Firebase', Color.YELLOW), '//firebase.google.com'),
        new Technology(new Skill('googleplayservices', 'GPServices', Color.GREEN), '//developer.android.com/distribute/play-services')
    ];
    images: Image[] = [];
}