import { Technology } from './technology.model';
import { Image } from './image.model';

export interface ProjectPage {
    id: string;
    title: string;
    url?: string;
    description: string[];
    technologies: Technology[];
    images: Image[];
}