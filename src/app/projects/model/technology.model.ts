import { Skill } from 'src/app/models/skill.model';

export class Technology {
  skill: Skill;
  url: string;

  constructor(skill: Skill, url: string) {
    this.skill = skill;
    this.url = url;
  }
}