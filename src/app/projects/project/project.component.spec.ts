import { async, ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';

import { ProjectComponent } from './project.component';
import { RouterModule, ActivatedRoute } from '@angular/router';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { faArrowLeft, faExternalLinkAlt } from '@fortawesome/free-solid-svg-icons';
import { ProjectPage } from '../model/project.model';
import { Technology } from '../model/technology.model';
import { Skill } from 'src/app/models/skill.model';
import { Color } from 'src/app/models/color.enum';
import { Image } from '../model/image.model';
import { SkillComponent } from 'src/app/skill/skill.component';

describe('ProjectComponent', () => {
  let component: ProjectComponent;
  let fixture: ComponentFixture<ProjectComponent>;
  let compiled: HTMLElement;

  const DUMMY_PROJECT_PAGE: ProjectPage = {
    id: 'testId',
    title: 'Test Page Title',
    url: 'https://testurl.com',
    description: [
      'test line 1',
      'test line 2'
    ],
    technologies: [
      new Technology(new Skill('angular', 'Test Technology Title', Color.GREEN, 50), 'https://testskillurl.com')
    ],
    images: [
      new Image('home', 'portfolio', 'Test Image Title')
    ]
  };
  
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProjectComponent, SkillComponent ],
      imports: [
        RouterModule.forRoot([]),
        FontAwesomeModule
      ],
      providers: [{
        provide: ActivatedRoute,
        useValue: {
          data: {
            subscribe: (fn: (value: ProjectPage) => void) => fn(DUMMY_PROJECT_PAGE)
          }
        }
      }]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProjectComponent);
    
    component = fixture.componentInstance;
    
    fixture.detectChanges();
    compiled = fixture.nativeElement;
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should have back icon', () => {
    expect(component.faBack).toEqual(faArrowLeft);
  });

  it('should have external link icon', () => {
    expect(component.faExternalLink).toEqual(faExternalLinkAlt);
  });

  it('should have a project page', () => {
    expect(component.projectPage).toBeTruthy();
  });

  it('should have a project page id', () => {
    expect(component.projectPage.id).toEqual(DUMMY_PROJECT_PAGE.id);
  });

  it('should have a project page title', () => {
    expect(component.projectPage.title).toEqual(DUMMY_PROJECT_PAGE.title);
  });

  it('should have a project page url', () => {
    expect(component.projectPage.url).toEqual(DUMMY_PROJECT_PAGE.url);
  });

  it('should have a project page description', () => {
    expect(component.projectPage.description.length).toEqual(DUMMY_PROJECT_PAGE.description.length);
    expect(component.projectPage.description).toEqual(DUMMY_PROJECT_PAGE.description);
  });

  it('should have project page technologies', () => {
    expect(component.projectPage.technologies.length).toEqual(DUMMY_PROJECT_PAGE.technologies.length);
    expect(component.projectPage.technologies).toEqual(DUMMY_PROJECT_PAGE.technologies);
  });

  it('should have project page images', () => {
    expect(component.projectPage.images.length).toEqual(DUMMY_PROJECT_PAGE.images.length);
    expect(component.projectPage.images).toEqual(DUMMY_PROJECT_PAGE.images);
  });

  it('should not have an open image', () => {
    expect(component.openedImage).toBeUndefined();
    expect(component.imageOpen).toBeFalse();
  });

  it('should correctly change opened image state to open', () => {
    expect(component.openedImage).toBeUndefined();
    expect(component.imageOpen).toBeFalse();

    expect(component.projectPage.images.length).toBeGreaterThan(0);
    component.openImage(component.projectPage.images[0]);

    expect(component.openedImage).toBe(component.projectPage.images[0]);
    expect(component.imageOpen).toBeTrue();
  });

  it('should correctly change opened image state to close', fakeAsync(() => {
    expect(component.projectPage.images.length).toBeGreaterThan(0);

    component.imageOpen = true;
    component.openedImage = component.projectPage.images[0];

    component.closeImage();

    expect(component.imageOpen).toBeFalse();

    tick(200);

    expect(component.openedImage).toBeNull();
  }));

  it('should render a project container', () => {
    expect(compiled.querySelector('.project-container')).toBeTruthy();
  });

  it('should render a header', () => {
    expect(compiled.querySelector('.project-container > .header')).toBeTruthy();
  });

  it('should render a back button in the header', () => {
    const button: HTMLElement = compiled.querySelector('.project-container > .header > #button-back');
    expect(button).toBeTruthy();
    expect(button.getAttribute('id')).toEqual('button-back');
    expect(button.getAttribute('href')).toEqual('/');
  });
  
});
