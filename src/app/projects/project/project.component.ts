import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ProjectPage } from '../model/project.model';
import { faArrowLeft, faExternalLinkAlt } from '@fortawesome/free-solid-svg-icons';
import { Image } from '../model/image.model';

@Component({
  selector: 'app-project',
  templateUrl: './project.component.html',
  styleUrls: ['./project.component.scss']
})
export class ProjectComponent implements OnInit {

  faBack = faArrowLeft;
  faExternalLink = faExternalLinkAlt;

  projectPage: ProjectPage;
  imageOpen: boolean = false;
  openedImage: Image;

  constructor(private activatedRoute: ActivatedRoute) { }

  ngOnInit(): void {
    this.activatedRoute.data.subscribe((data: ProjectPage) => {
      this.projectPage = data;
    });
  }

  openImage(image: Image): void {
    this.openedImage = image;
    this.imageOpen = true;
  }

  closeImage(): void {
    this.imageOpen = false;
    setTimeout(() => {
      this.openedImage = null;
    }, 200);
  }

}
