import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';

import { ProjectComponent } from './project/project.component';
import { SkillComponent } from '../skill/skill.component';

import { PortfolioPage } from './model/pages/portfolio.page';
import { MyMahiPage } from './model/pages/mymahi.page';
import { TheGraphicShopPage } from './model/pages/thegraphicshop';
import { MinecraftPage } from './model/pages/minecraft.page';
import { ExpressJSPage } from './model/pages/expressjs.page';
import { SudokuSolverPage } from './model/pages/sudokusolver.page';
import { TukBoxPage } from './model/pages/tukbox.page';

const routes: Routes = [{
  path: 'portfolio', component: ProjectComponent,
  data: new PortfolioPage()
}, {
  path: 'mymahi', component: ProjectComponent,
  data: new MyMahiPage()
}, {
  path: 'thegraphicshop', component: ProjectComponent,
  data: new TheGraphicShopPage()
// }, {
//   path: 'harleycartel', component: ProjectComponent,
//   data: new PortfolioPage()
}, {
  path: 'minecraft', component: ProjectComponent,
  data: new MinecraftPage()
}, {
  path: 'expressjs', component: ProjectComponent,
  data: new ExpressJSPage()
}, {
  path: 'sudokusolver', component: ProjectComponent,
  data: new SudokuSolverPage()
}, {
  path: 'tukbox', component: ProjectComponent,
  data: new TukBoxPage()
}];

@NgModule({
  declarations: [ProjectComponent, SkillComponent],
  exports: [SkillComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    FontAwesomeModule
  ]
})
export class ProjectsModule { }
