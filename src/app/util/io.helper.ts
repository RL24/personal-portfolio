export const IMAGES_DIRECTORY = '/assets/images';
export const CERTIFICATIONS_DIRECTORY = `${IMAGES_DIRECTORY}/certifications`;
export const GALLERY_DIRECTORY = `${IMAGES_DIRECTORY}/gallery`;
export const PROJECTS_DIRECTORY = `${IMAGES_DIRECTORY}/projects`;
export const TOOLS_DIRECTORY = `${IMAGES_DIRECTORY}/tools`;

export const getImage = (name: string) => {
    return `${IMAGES_DIRECTORY}/${name}.webp`;
};

export const getCertification = (name: string) => {
    return `${CERTIFICATIONS_DIRECTORY}/${name}.webp`;
}

export const getGalleryImage = (folder: string, thumbnail: boolean, name: string) => {
    return `${GALLERY_DIRECTORY}/${folder}${thumbnail ? '/thumbnail' : ''}/${name}.webp`;
}

export const getProject = (name: string) => {
    return `${PROJECTS_DIRECTORY}/${name}.webp`;
};

export const getTool = (name: string) => {
    return `${TOOLS_DIRECTORY}/${name}.webp`;
}