window.onload = () => {
    $('.radial-progress').each((i, el) => {
        const $el = $(el);
        const progress = $el.attr('progress') || 0;
        const color = $el.attr('color') || 'grey';

        var line = new ProgressBar.Circle(el, {
            color: `rgba(${color}, 1)`,
            trailColor: `rgba(${color}, 0.2)`,
            duration: 1400,
            easing: 'easeInOut',
            strokeWidth: 8
        });
        line.animate((1 / 100) * progress);
    });
};